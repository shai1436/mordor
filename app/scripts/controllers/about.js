'use strict';

/**
 * @ngdoc function
 * @name mordorApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the mordorApp
 */
angular.module('mordorApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
