'use strict';

/**
 * @ngdoc function
 * @name mordorApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the mordorApp
 */
angular.module('mordorApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = "data";
  });
