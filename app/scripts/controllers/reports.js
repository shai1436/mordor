'use strict';

/**
 * @ngdoc function
 * @name mordorApp.controller:ReportCtrl
 * @description
 * # AboutCtrl
 * Controller of the mordorApp
 */
angular.module('mordorApp')
  .controller('ReportsCtrl', ['ReportsServ', '$http', '$scope', function (ReportsServ, $http, $scope) {

    this.route = 'http://localhost:3000/categories';
    $scope.dataList={};
    $http.get(this.route).then(
      function(response) {

        $scope.dataList= response.data;

        console.log($scope.dataList[1]['agriculture'][0]);
      },
      function(err){
        console.log(err);
      }
    );

  }]);
