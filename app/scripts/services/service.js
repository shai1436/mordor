'use strict';

/**
 * @ngdoc function
 * @name mordorApp.service:ReportServ
 * @description
 * # AboutCtrl
 * Controller of the mordorApp
 */
angular.module('mordorApp')
  .constant('baseURL','http://localhost:3000/')
  .service('ReportsServ', ['$http', '$resource', 'baseURL', function ($http, $resource, baseURL) {
    this.getData = function (path) {
      var data ={};
      if(path){
          data =  $resource(path);
      }
      else{
          data =  $resource(baseURL);
      }
      console.log(data);
      return data;
    };
  }]);
